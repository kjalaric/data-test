'''
data_generator.py

Generates data for algorithm testing
kjalaric@gitlab

'''

import numpy as np
from mercurial.templater import separate

class DataGenerationError(Exception):
    '''
    generic data generation error in data_generator.py
    '''
    pass

def bounds_check(observation, min_value, max_value):
    '''
    returns True if observation is between min_value and max_value, false otherwise
    '''
    if (observation > max_value) or (observation < min_value):
        return False
    return True

def get_x_and_y(data_as_list_of_tuples):
    '''
    converts a list of tuples to a list of x and a list of y
    '''
    x, y = zip(*data_as_list_of_tuples)
    return x, y

def two_clusters(cluster_size=10, min_value=0, max_value=100, std_dev=5, separate_lists=False):
    '''
    return a list (length = 2*cluster_size) of tuples, each representing an
    (x,y) point in one of two clusters (each with cluster_size points)
    cluster means add to 100 in both axes. clusters are normally-
    distributed but the effective std_dev may be smaller
    as cluster x and y values are rejected if greater than
    max_value or lower than min_value
    
    if separate_lists=True, returns two lists, each containing separated cluster values
    otherwise, all points are in the same single return list
    '''
    upper_bound = max_value-3*std_dev
    lower_bound = min_value+3*std_dev
    
    # check to see if random.uniform bounds will be OK
    if upper_bound <= lower_bound:
        raise DataGenerationError("two_clusters(): upper bound < lower bound")
    
    # means
    cluster1x = np.random.uniform(lower_bound, upper_bound)
    cluster1y = np.random.uniform(lower_bound, upper_bound)
    cluster2x = max_value - cluster1x
    cluster2y = max_value - cluster1y
    
    data = list()
    if separate_lists:
        data.append(list())
        data.append(list())
    
    # begin generation
    for i in range(cluster_size):
        c1x = np.random.normal(cluster1x, std_dev)
        while not bounds_check(c1x, min_value, max_value):
            c1x = np.random.normal(cluster1x, std_dev)
        
        c1y = np.random.normal(cluster1y, std_dev)
        while not bounds_check(c1y, min_value, max_value):
            c1y = np.random.normal(cluster1y, std_dev) 

        c2x = np.random.normal(cluster2x, std_dev)
        while not bounds_check(c2x, min_value, max_value):
            c2x = np.random.normal(cluster2x, std_dev)
        
        c2y = np.random.normal(cluster2y, std_dev)
        while not bounds_check(c2y, min_value, max_value):
            c2y = np.random.normal(cluster2y, std_dev) 
        
        if separate_lists:
            data[0].append((c1x, c1y))
            data[1].append((c2x, c2y))
        else:
            data.append((c1x, c1y))
            data.append((c2x, c2y))

    return data

    